import { useCallback, useEffect, useState, useMemo } from 'react';

import OrderBook from '../Orderbook';
import { Products, productConfig } from '../../utils/constants';

import styles from '../../../styles/Orderbook.module.css';
import 'font-awesome/css/font-awesome.min.css';

type OrdersType = [number, number][];
type AllOrders = {
  asks: OrdersType;
  bids: OrdersType;
} | null;

function App() {
  const [orders, setOrders] = useState<AllOrders>(null);
  const [productId, setProductId] = useState(Products.PI_XBTUSD);
  const [isSocketOpen, setSocketState] = useState(false);
  const [isSocketActive, setIsSocketActive] = useState(true);
  const [isFirstTime, setIsFirstTime] = useState(true);
  const marketInfo = useMemo(() => productConfig[productId], [productId]);
  const [dropDownOpen, setDropdownOpen] = useState(false);
  const [group, setGroup] = useState(marketInfo.defaultTick);

  const webSocket = useMemo(
    () => new WebSocket('wss://www.cryptofacilities.com/ws/v1'),
    [isSocketOpen]
  );

  function openSocket() {
    setSocketState((prev) => !prev);
  }

  function closeSocket() {
    webSocket.close();
    setIsSocketActive(false);
  }

  const subscribe = useCallback(() => {
    if (!webSocket) return;
    const msg = {
      event: 'subscribe',
      feed: 'book_ui_1',
      product_ids: [productId],
    };
    if (webSocket.readyState === 1) {
      webSocket.send(JSON.stringify(msg));
    }
  }, [productId, webSocket]);

  const unsubscribe = useCallback(() => {
    const msg = {
      event: 'unsubscribe',
      feed: 'book_ui_1',
      product_ids: [productId],
    };
    webSocket.send(JSON.stringify(msg));
  }, [productId, webSocket]);

  function handleChange(orderBookGroup: number) {
    setGroup(orderBookGroup);
    setDropdownOpen((prev) => !prev);
  }

  function handleToggleMarket() {
    unsubscribe();
    if (productId === Products.PI_XBTUSD) {
      return setProductId(Products.PI_ETHUSD);
    }
    return setProductId(Products.PI_XBTUSD);
  }

  function toggleConnection() {
    return webSocket.readyState === 3 ? openSocket() : closeSocket();
  }

  useEffect(
    () => () => {
      closeSocket();
    },
    []
  );

  useEffect(() => {
    if (isFirstTime) {
      setIsFirstTime(!isFirstTime);
    } else {
      subscribe();
    }
  }, [productId]);

  useEffect(() => {
    webSocket.onopen = () => {
      subscribe();
      setIsSocketActive(true);
    };

    webSocket.onerror = () => {
      setIsSocketActive(false);
      closeSocket();
    };

    webSocket.onmessage = (event: MessageEvent) => {
      const response = JSON.parse(event.data);
      setOrders(response);
    };
  }, [webSocket]);

  useEffect(() => {
    setGroup(marketInfo.defaultTick);
  }, [marketInfo]);

  return (
    <div className="container-md">
      <div className={`row ${styles.header}`}>
        <div className="col-6 align-content-lg-start mt-1">Order book</div>
        <div className="col-6 text-end">
          <div className={`dropdown ${styles.ddMenu}`}>
            <button
              className="btn btn-secondary dropdown-toggle"
              type="button"
              id="dropdownMenuButton1"
              data-bs-toggle="dropdown"
              aria-expanded="false"
              onClick={() => setDropdownOpen((prevState) => !prevState)}
            >
              {`Group ${group}`}
            </button>
            <ul
              className={`dropdown-menu ${dropDownOpen ? 'show' : ''}`}
              aria-labelledby="dropdownMenuButton1"
            >
              {marketInfo.ticks.map((orderBookGroup: number) => (
                <li
                  key={orderBookGroup}
                  onClick={() => handleChange(orderBookGroup)}
                  value={orderBookGroup}
                >
                  <a
                    className="dropdown-item"
                    href="#"
                  >{`Group ${orderBookGroup}`}</a>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
      <OrderBook orders={orders} product={productId} group={group} />
      <div className="row mt-2">
        <div className="d-flex justify-content-center">
          <button
            disabled={!isSocketActive}
            onClick={handleToggleMarket}
            className="btn"
            style={{
              backgroundColor: '#5841D9',
              color: '#ADA6E3',
              textAlign: 'right',
              margin: '10px',
            }}
          >
            <i className="fa fa-exchange" /> Toggle feed
          </button>
          <button
            onClick={toggleConnection}
            color={isSocketActive ? 'secondary' : 'primary'}
            className="btn"
            style={{
              backgroundColor: '#B92A25',
              color: '#D9B4B7',
              textAlign: 'left',
              margin: '10px',
            }}
          >
            <i className="fa fa-exclamation-circle" />{' '}
            {isSocketActive ? 'Kill Feed' : 'Restart feed'}
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
