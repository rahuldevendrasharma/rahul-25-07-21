import { useMemo } from 'react';

import OrderBookList from '../OrderbookList';
import { Products, productConfig } from '../../utils/constants';

import styles from '../../../styles/Orderbook.module.css';

type OrdersType = [number, number][];
type AllOrders = {
  asks: OrdersType;
  bids: OrdersType;
} | null;

type OrderBookType = {
  orders: AllOrders;
  product: keyof typeof Products;
  group: number;
};

function OrderBook({ orders, product, group }: OrderBookType) {
  const productInfo = useMemo(() => productConfig[product], [product]);

  return (
    <div>
      <div
        className={`row ${styles['order-book']}`}
        style={{
          padding: '0 5px 5px 5px',
        }}
      >
        {orders && orders.bids && orders.asks && (
          <>
            <div className="col-md-6 p-0 order-2 order-md-1">
              <OrderBookList
                orders={orders.asks}
                type="asks"
                group={group}
                defaultGroup={productInfo.defaultTick}
              />
            </div>
            <div className="col-md-6 p-0 order-1 order-md-2">
              <OrderBookList
                orders={orders.bids}
                type="bids"
                group={group}
                defaultGroup={productInfo.defaultTick}
              />
            </div>
          </>
        )}
      </div>
    </div>
  );
}

export default OrderBook;
