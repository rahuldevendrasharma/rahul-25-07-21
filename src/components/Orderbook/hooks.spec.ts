import { renderHook, act } from '@testing-library/react-hooks';

import { resizeTo } from '../../../test/testUtils';
import { useWindowDimensions } from './hooks';

const renderWindowSize = () => renderHook(() => useWindowDimensions());

describe('useWindowDimensions()', () => {
  it('should update on resize events', () => {
    act(() => resizeTo(600, 400));
    const { result } = renderWindowSize();
    expect(result.current.width).toBe(600);
    expect(result.current.height).toBe(400);
    act(() => resizeTo(1280, 720));
    expect(result.current.width).toBe(1280);
    expect(result.current.height).toBe(720);
  });
});
