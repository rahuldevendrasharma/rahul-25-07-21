import { render } from '../../../test/testUtils';
import Orderbook from './Orderbook';

const props: any = {
  orders: {
    asks: [[34502, 137203]],
    bids: [[34502, 137203]],
    feed: 'mock_value',
    product_id: 'PI_XBTUSD',
  },
  product: 'PI_XBTUSD',
  group: 0.5,
};

describe('Orderbook', () => {
  it('should render correctly', () => {
    const { asFragment } = render(<Orderbook {...props} />, {});
    expect(asFragment()).toMatchSnapshot();
  });
});
