import {
  getOrdersByPrice,
  getGroupedOrdersByPrice,
  getOrdersArray,
} from './utils';

const orders: any = [
  [300.5, 25],
  [400.5, 50],
];
const ordersByPrice = {
  '300.5': { price: 300.5, size: 25 },
  '400.5': { price: 400.5, size: 50 },
};

const ordersByPriceForGroup = {
  '303.5': { price: 303.5, size: 25 },
  '403.5': { price: 403.5, size: 50 },
};

const ordersByPriceForFirstGroup = {
  '303': { price: 303, size: 25 },
  '403': { price: 403, size: 50 },
};

const ordersByPriceForLastGroup = {
  '302.5': { price: 302.5, size: 25 },
  '402.5': { price: 402.5, size: 50 },
};

const finalisedOrders = [
  {
    price: 303,
    size: 25,
    total: 25,
  },
  {
    price: 403,
    size: 50,
    total: 75,
  },
];

describe('getOrdersByPrice', () => {
  it('should return orders by price when fed with correct orders', () => {
    const resultantOrders = getOrdersByPrice(orders, {});
    expect(resultantOrders).toEqual(ordersByPrice);
  });

  it('should return empty orders by price when fed with no orders', () => {
    const resultantOrders = getOrdersByPrice(
      [
        [0, 0],
        [0, 0],
      ],
      {}
    );
    expect(resultantOrders).toEqual({});
  });
});

describe('getGroupedOrdersByPrice', () => {
  it('should return grouped orders by price when fed with group 1', () => {
    const resultantOrders = getGroupedOrdersByPrice(ordersByPriceForGroup, 1);
    expect(resultantOrders).toEqual(ordersByPriceForFirstGroup);
  });

  it('should return grouped orders by price when fed with group 2.5', () => {
    const resultantOrders = getGroupedOrdersByPrice(ordersByPriceForGroup, 2.5);
    expect(resultantOrders).toEqual(ordersByPriceForLastGroup);
  });
});

describe('getOrdersArray', () => {
  it('should return orders array when fed with correct grouped orders by price', () => {
    const resultantOrders = getOrdersArray(ordersByPriceForFirstGroup);
    expect(resultantOrders).toEqual(finalisedOrders);
  });
});
