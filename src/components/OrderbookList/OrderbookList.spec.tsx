import { render } from '../../../test/testUtils';
import OrderbookList from './OrderbookList';

const props: any = {
  orders: [
    [300, 25],
    [400, 50],
  ],
  type: 'asks',
  group: 0.5,
  defaultGroup: 0.5,
};

describe('OrderbookList', () => {
  it('should render correctly', () => {
    const { asFragment } = render(<OrderbookList {...props} />, {});
    expect(asFragment()).toMatchSnapshot();
  });
});
