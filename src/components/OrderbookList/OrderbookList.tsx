import { useCallback, useEffect, useMemo, useState } from 'react';
import {
  getGroupedOrdersByPrice,
  getOrdersArray,
  getOrdersByPrice,
} from './utils';
import { useWindowDimensions } from '../Orderbook/hooks';
import { orderListConfig, OrderBookValueList } from '../../utils/constants';

import styles from '../../../styles/OrderbookList.module.css';

type SingleOrderType = {
  ['price']: number;
  ['size']: number;
  ['total']: number;
};

type OrdersType = [number, number][];

type OrderBookListPropsType = {
  orders: OrdersType;
  type: 'asks' | 'bids';
  group: number;
  defaultGroup: number;
};

type OrdersByPriceType = Record<
  string,
  {
    ['price']: number;
    ['size']: number;
  }
>;

const mobileTitles = ['price', 'size', 'total'];

function OrderBookList({
  orders,
  type,
  group,
  defaultGroup,
}: OrderBookListPropsType) {
  const { color, mobileColor, depthColor, titles, direction } = useMemo(
    () => orderListConfig[type],
    [type]
  );
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { height, width } = useWindowDimensions();

  const isMobileView: boolean = width <= 768;

  const flipForMobile = isMobileView && type === 'bids';
  const [ordersByPrice, setOrdersByPrice] = useState<OrdersByPriceType>({});

  useEffect(() => {
    setOrdersByPrice((prevOrders) => getOrdersByPrice(orders, prevOrders));
  }, [orders, setOrdersByPrice]);

  const getMarketDepthBackground = useCallback(
    (marketDepth: number) =>
      `linear-gradient(${
        flipForMobile ? '-90deg' : direction
      }, ${depthColor} ${marketDepth}%, transparent ${marketDepth}%)`,
    [direction, depthColor, flipForMobile]
  );

  const groupedOrdersByPrice = useMemo(() => {
    if (group === defaultGroup) {
      return ordersByPrice;
    }
    return getGroupedOrdersByPrice(ordersByPrice, group);
  }, [ordersByPrice, group, defaultGroup]);

  const ordersArr: SingleOrderType[] = useMemo(() => {
    const orders = getOrdersArray(groupedOrdersByPrice);
    return flipForMobile ? orders.reverse() : orders;
  }, [groupedOrdersByPrice, flipForMobile]);

  const orderedTitles = isMobileView ? mobileTitles : titles;
  const colour = isMobileView ? mobileColor : color;

  const getTableRow = useCallback(
    (order: SingleOrderType, marketDepth: number) => (
      <tr
        key={`${order.price}`}
        style={{
          background: getMarketDepthBackground(marketDepth),
        }}
      >
        {orderedTitles.map((tableKey) => (
          <td
            className={styles.cell}
            key={tableKey}
            style={{
              color: tableKey === OrderBookValueList.PRICE ? colour : 'white',
            }}
          >
            {order[tableKey as OrderBookValueList].toLocaleString()}
          </td>
        ))}
      </tr>
    ),
    [getMarketDepthBackground, orderedTitles, colour]
  );

  return (
    <table className="table">
      <thead
        className={
          styles.tableHead +
          ' ' +
          (type === 'asks' && isMobileView ? 'visually-hidden' : '')
        }
      >
        <tr>
          {orderedTitles.map((title) => (
            <th className={styles.headerCell} key={title}>
              {title}
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {ordersArr.map((order) => {
          let marketDepth = Math.round(
            (order.total / ordersArr[ordersArr.length - 1].total) * 100
          );
          if (flipForMobile) {
            marketDepth = Math.round((order.total / ordersArr[0].total) * 100);
          }
          return getTableRow(order, marketDepth);
        })}
      </tbody>
    </table>
  );
}

export default OrderBookList;
