enum Products {
  PI_XBTUSD = 'PI_XBTUSD',
  PI_ETHUSD = 'PI_ETHUSD',
}

const productConfig = {
  ['PI_XBTUSD']: {
    defaultTick: 0.5,
    ticks: [0.5, 1, 2.5],
  },
  ['PI_ETHUSD']: {
    defaultTick: 0.05,
    ticks: [0.05, 0.1, 0.25],
  },
};

const orderListConfig = {
  ['asks']: {
    color: '#04986A',
    depthColor: '#3F212C',
    mobileColor: '#EF4444',
    titles: ['total', 'size', 'price'],
    direction: '-90deg',
  },
  ['bids']: {
    color: '#04986A',
    mobileColor: '#04986A',
    depthColor: '#13393A',
    titles: ['price', 'size', 'total'],
    direction: '90deg',
  },
};

enum OrderBookValueList {
  PRICE = 'price',
  SIZE = 'size',
  TOTAL = 'total',
}

export { Products, productConfig, orderListConfig, OrderBookValueList };
