import type { AppProps } from 'next/app';
import dynamic from 'next/dynamic';
import React from 'react';

import '../styles/globals.css';

const App = ({ Component, pageProps }: AppProps) => {
  return <Component {...pageProps} />;
};

// Disable SSR for websockets
export default dynamic(() => Promise.resolve(App), {
  ssr: false,
});
