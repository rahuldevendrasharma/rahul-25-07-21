import Head from 'next/head';
import App from '../src/components/App';
import 'bootstrap/dist/css/bootstrap.css';

const IndexPage = () => (
  <div className="container">
    <Head>
      <title>Order book</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <App />
  </div>
);

export default IndexPage;
